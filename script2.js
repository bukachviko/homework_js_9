const arr1 = ["hello", "world", "Kiev", "Kharkiv", ["1", "2", "sea", "user", 23], "Odessa", "Lviv"];

function getListContent(arrList) {
    const ul = document.createElement('ul');
    let fragment = new DocumentFragment();

    for(let i=0; i < arrList.length; i++) {
        if(Array.isArray(arrList[i])) {
            fragment.append(getListContent(arrList[i]));
            continue;
        }

        let li = document.createElement('li');
        li.append(arrList[i]);
        fragment.append(li);
    }
    ul.append(fragment);

    return ul;
}

document.querySelector('body').prepend(getListContent(arr1));




